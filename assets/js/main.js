$(function(){
   
    $('.scrollto').on('click',function(){
        var el = $(this).attr('href');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(el).offset().top
        }, 1500);        
    });

    (function(){

        var form = $('form');

        form.on('submit',function(e){

            e.preventDefault();            

            $.ajax({
                type: 'POST',
                url: "../send.php",
                data: form.serialize(),
                cache: false,
                success: function(data){                        
                },
                statusCode: {
                    200: function(){
                        form.addClass('d-none');
                        $('.result').removeClass('d-none');
                    },
                    404: function(){
                        console.log('something is wrong');
                    }
                }
            });

        });

    })();
    

});